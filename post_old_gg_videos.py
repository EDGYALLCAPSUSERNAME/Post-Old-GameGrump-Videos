import time
import praw
import dataset
import OAuth2Util

SUBREDDIT_NAME = ''


def find_old_videos_from_todays_date(r, db):
    month = str(int(time.strftime("%m")))
    day = time.strftime("%d")
    videos = db.query("SELECT * FROM links WHERE date LIKE '{}/{}/20%'".format(month, day))

    for video in videos:
        d = dict(video)
        post_video_to_subreddit(r, d)


def post_video_to_subreddit(r, d):
    subreddit = r.get_subreddit(SUBREDDIT_NAME)
    post_title = "[{}] {}".format(d['date'], d['title'])

    subreddit.submit(post_title, url=d['link'])


def main():
    r = praw.Reddit('Post_Old_GG_Videos v1.0 /u/EDGYALLCAPSUSERNAME')
    o = OAuth2Util.OAuth2Util(r, print_log=True)
    db = dataset.connect('sqlite:///game_grump_links.db')

    find_old_videos_from_todays_date(r, db)

if __name__ == "__main__":
    main()
