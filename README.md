Posts Old Game Grump Videos
===========================

Setup
-----

This script requires python 3 and praw, praw-OAuth2Util, and dataset.

To install praw type in your command line:

    pip install praw
    pip install praw-oauth2util
    pip install dataset


Config
------

Add the name of the subreddit you want to post the videos to, to the SUBREDDIT_NAME variable.